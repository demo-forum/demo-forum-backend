from setuptools import setup, find_packages

setup(
    name='demo-forum-backend',
    version='0.1.0',
    packages=find_packages(),
    url='https://gitlab.com/demo-forum/demo-forum-backend',
    license='MIT',
    author='Kevin Ju',
    description='A demo forum\'s backend API.'
)
